//include express framework
const express = require("express");

// instantiate the express app
const app = express();

//import cors to allow cross-origin requests
const cors = require("cors");

//add body parser for JSON content types
const bodyParser = require("body-parser");

//configure express to use a middleware called body-parser for analyzing/parsing the body of our request
app.use(bodyParser.json());

//allow cross-origin requests
app.use(cors());

const index = require("./routes/index");
app.use("/", index)

//listen method - listen.(port, callback function)
app.listen(8000, () => {
	console.log("server is running on port 8000")
});